//
// Created by ivaughn on 8/18/19.
//

#ifndef DS_SENSORS_UBLOX_RTK_H_
#define DS_SENSORS_UBLOX_RTK_H_

#include <ds_base/sensor_base.h>
#include <memory>

#include <ublox_rtk/SetRtkMode.h>

namespace ds_sensors {

struct UbloxGpsPrivate;

class UbloxGps : public ds_base::SensorBase
    {
  DS_DECLARE_PRIVATE(UbloxGps);

 public:
  explicit UbloxGps();
  UbloxGps(int argc, char* argv[], const std::string& name);
  ~UbloxGps() override;
  DS_DISABLE_COPY(UbloxGps);

 protected:
  void setup() override;
  void setupConnections() override;
  void setupPublishers() override;
  void setupParameters() override;
  void setupServices() override;

  void parseReceivedBytes(const ds_core_msgs::RawData& bytes) override;

  void sendOutputConfig();

 private:
  std::unique_ptr<UbloxGpsPrivate> d_ptr_;

  void _onCorrectionMsg(ds_core_msgs::RawData in);
  void _handleRtcm(const ds_core_msgs::RawData& bytes);
  bool _setMode(ublox_rtk::SetRtkModeRequest& req, ublox_rtk::SetRtkModeResponse& resp);
  void _setRover(bool rover);
};

} // namespace ds_sensors

#endif // DS_SENSORS_UBLOX_RTK_H_
