//
// Created by ivaughn on 8/18/19.
//

#include <ublox_rtk/ublox_rtk.h>
#include <memory>

int main(int argc, char* argv[]) {
  // start ROS
  ros::init(argc, argv, "ublox_gps");
  
  // Create our node
  auto node = std::unique_ptr<ds_sensors::UbloxGps>(new ds_sensors::UbloxGps());
  
  node->run();
  
  return 0;
}