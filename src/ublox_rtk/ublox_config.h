//
// Created by ivaughn on 8/22/19.
//

#ifndef RTK_UBLOX_CONFIG_H
#define RTK_UBLOX_CONFIG_H

#include "ubx.h"

#include <string>

namespace ublox {

// Max number of variables is 64, which requires 256 bytes of
// keys
#define RTK_UBX_MAX_CONFIG_MSGLEN 512

#define RTK_UBX_MAX_KEYS 64

class UbloxConfig {

 public:
  UbloxConfig();
  UbloxConfig(uint8_t layer);
  ~UbloxConfig() = default;

  // T is always: uint8_t, uint16_t, or uint32_t
  template<typename T>
  void addConfigValue(uint32_t key, T value);

  void reset(uint8_t layer);
  std::string toMessage(); // fills in length/checksum, but can continue adding stuff after

  const void* data();
  size_t size();
  size_t keys();

 private:

  // this sets up a memory block that's big enough for a large config message
  // and has headers and everything pointing into it.
  union {
    struct {
      struct ubx_hdr hdr;
      struct ubx_cfg_hdr cfg_hdr;
    } __attribute__((packed));
    uint8_t buffer[RTK_UBX_MAX_CONFIG_MSGLEN];
  };

  uint8_t* ptr;
  size_t len;
  size_t keys_;
};

} //namespace ublox

#endif //RTK_UBLOX_CONFIG_H
