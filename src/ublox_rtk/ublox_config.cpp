//
// Created by ivaughn on 8/22/19.
//

#include "ublox_config.h"

#include <stdexcept>
#include <boost/endian/conversion.hpp>

namespace ublox {

UbloxConfig::UbloxConfig() {
  reset(UBX_CFG_LAYER_RAM);
}

UbloxConfig::UbloxConfig(uint8_t target_layer) {
  reset(target_layer);
}

template<typename T>
void UbloxConfig::addConfigValue(uint32_t key, T value) {
  if (keys_ >= RTK_UBX_MAX_KEYS) {
    throw std::runtime_error("Cannot add more than 32 keys to one UBlox config message!");
  }

  if (len + sizeof(uint32_t) + sizeof(T) + UBX_CHECKSUM_LEN > RTK_UBX_MAX_CONFIG_MSGLEN) {
    throw std::runtime_error("Tried to add too much data to UBlox config message!");
  }

  // add the key
  *reinterpret_cast<uint32_t*>(ptr) = boost::endian::native_to_little(key);
  ptr += sizeof(uint32_t);
  len += sizeof(uint32_t);

  *reinterpret_cast<T*>(ptr) = boost::endian::native_to_little(value);
  ptr += sizeof(T);
  len += sizeof(T);

  keys_++;
}
template void UbloxConfig::addConfigValue(uint32_t key, uint8_t value);
template void UbloxConfig::addConfigValue(uint32_t key, uint16_t value);
template void UbloxConfig::addConfigValue(uint32_t key, uint32_t value);

void UbloxConfig::reset(uint8_t layer) {
  memset(buffer, 0, sizeof(buffer));

  len = sizeof(ubx_hdr) + sizeof(ubx_cfg_hdr);
  ptr = buffer + len;
  keys_ = 0;

  // initialize the structure
  hdr.sync1 = UBX_SYNC1;
  hdr.sync2 = UBX_SYNC2;
  hdr.msg_class = UBX_CFG_CLASS;
  hdr.msg_id = UBX_CFG_ID_VALSET;

  cfg_hdr.version = UBX_CFG_VERSION;
  cfg_hdr.layer = layer;
}

std::string UbloxConfig::toMessage() {
  hdr.payload_size = boost::endian::native_to_little(len - sizeof(ubx_hdr));

  *reinterpret_cast<uint16_t*>(buffer + len) = ubx_calc_checksum(buffer, len);

  std::string ret(reinterpret_cast<char*>(buffer), len + sizeof(uint16_t));

  return ret;

}

const void* UbloxConfig::data() {
  return reinterpret_cast<void*>(buffer);
}

size_t UbloxConfig::size() {
  return len;
}

size_t UbloxConfig::keys() {
  return keys_;
}

} // namespace ublox
