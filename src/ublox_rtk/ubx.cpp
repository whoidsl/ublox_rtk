//
// Created by ivaughn on 8/17/19.
//

#include "ubx.h"

#include <stdint.h>
#include <string.h>
#include <endian.h>

uint16_t ubx_calc_checksum(const uint8_t *const buf, size_t len) {
  uint8_t ck_a=0, ck_b=0;
  for (size_t i=2; i<len; i++) {
    ck_a += buf[i];
    ck_b += ck_a;
  }

  // we want ck_a first in memory-- so to be portable, we'll use an endian call
  return htobe16((ck_a << 8) | ck_b);
}

