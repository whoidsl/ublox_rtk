//
// Created by ivaughn on 8/18/19.
//

#ifndef UBLOX_RTK_RTCM_H_
#define UBLOX_RTK_RTCM_H_

#include <stdint.h>

// a few basic definitions describing how RTCM packets are put together

#define RTCM_SYNC 0xd3
#define RTCM_SYNC_SIGNED -45

// this is a REALLY annoying structure to deal with
struct rtcm_hdr {
  uint8_t sync;
  uint16_t  length; // this field is actually only 10 bits, but... sigh.
} __attribute__((packed));

#define RTCM_CHECKSUM_LEN 3

#endif //UBLOX_RTK_RTCM_H_
