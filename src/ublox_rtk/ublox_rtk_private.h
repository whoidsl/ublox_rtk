//
// Created by ivaughn on 8/18/19.
//

#ifndef DS_SENSORS_UBLOX_RTK_PRIVATE_H_
#define DS_SENSORS_UBLOX_RTK_PRIVATE_H_

#include <ublox_rtk/ublox_rtk.h>
#include <ds_sensor_msgs/UbloxNav.h>
#include <ds_sensor_msgs/UbloxSatellites.h>
#include <ds_sensor_msgs/UbloxSignals.h>
#include <ds_sensor_msgs/UbloxSurveyIn.h>
#include <sensor_msgs/NavSatFix.h>
#include <geometry_msgs/PointStamped.h>

#include "ubx.h"

namespace ds_sensors {

struct UbloxGpsPrivate {

  // ROS publishers

  /// \brief Publishes a standard NavSat message
  ros::Publisher navsat_pub;

  /// \brief Publishes the full UBX nav message
  ros::Publisher ubx_nav_pub;

  /// \brief Publishes the UBX satellite message
  ros::Publisher ubx_sat_pub;

  /// \brief Publishes the UBX signal message
  ros::Publisher ubx_sig_pub;

  /// \brief Publishers the UBX survey-in message, useful for
  ros::Publisher ubx_svin_pub;

  /// \brief Publishes a geometry_msg/Point3D of the GPS's position relative to
  /// the base station.  Does nothing in base station mode
  ros::Publisher rover_point;

  /// \brief If true, this GPS is operating as a mobile rover getting
  /// corrections from a fixed base station
  bool rover;

  /// \brief This message gets filled in as more data comes in
  ds_sensor_msgs::UbloxNav big_msg;

  /// \brief The TF frame that goes in navsat_pub and ubx_pub
  std::string frame_id;

  /// \brief The TF frame of the base station.
  std::string base_station_id;

  /// \brief Connection for RTCM
  boost::shared_ptr<ds_asio::DsConnection> corrections;

  /// \brief Time the most recent RTCM correction arrived
  ros::Time latest_correction_time;

  // note: instrument connection is implied by base class

  /// \brief Service call to switch between base and rover mode
  ros::ServiceServer mode_service;

  /// \brief Base sample interval, in milliseconds
  uint16_t base_measurement_interval;

  /// \brief Rate at which to send NMEA out on port 2
  /// This is a downsample rate from the base rate
  uint8_t port2_nmea_downsample;

  /// \brief UBX update rate (how fast ROS driver publishes)
  /// This is a downsample rate from the base rate
  uint8_t update_downsample;

  /// \brief UBX downsample rate for satellit and svin info.
  /// This is a downsample rate from the base rate
  uint8_t sat_downsample;

  /// \brief RTCM update rate (how fast ROS driver publishes)
  /// This is a downsample rate from the base rate
  /// Should probably match
  uint8_t rtcm_downsample;

  /// \brief Disable rtcm on port2.  Off by default.
  bool port2_rtcm_disable;

  /// \brief Layer to write the configuration into.  One of:
  /// 0 -- NONE (do not write config)
  /// UBX_CFG_LAYER_RAM 0x01
  /// UBX_CFG_LAYER_BBR 0x02
  /// UBX_CFG_LAYER_FLASH 0x04
  uint8_t config_layer;

  uint32_t host_port;
  float svin_duration;
  float svin_accuracy;

  void printUbx(const ds_core_msgs::RawData& bytes) {
    const ubx_hdr* hdr = reinterpret_cast<const ubx_hdr*>(bytes.data.data());

    std::string msg_class, msg_type;

    const uint16_t calc_checksum = ubx_calc_checksum(bytes.data.data(), hdr->payload_size + sizeof(ubx_hdr));
    const uint16_t recv_checksum = *reinterpret_cast<const uint16_t*>(
        bytes.data.data() + hdr->payload_size + sizeof(ubx_hdr));

    std::stringstream c;
    c<<static_cast<int>(hdr->msg_class);
    msg_class = c.str();

    std::stringstream t;
    t<<static_cast<int>(hdr->msg_id);
    msg_type = t.str();

    switch(hdr->msg_class) {
      case UBX_NAV_CLASS:
        msg_class = "nav";
        {
          switch (hdr->msg_id) {
            case UBX_NAV_ID_PVT: msg_type="pvt"; break;
            case UBX_NAV_ID_HPPOSLLH: msg_type="hpposllh"; break;
            case UBX_NAV_ID_SAT: msg_type="sat"; break;
            case UBX_NAV_ID_SIG: msg_type="sig"; break;
            case UBX_NAV_ID_SVIN: msg_type="svin"; break;
            case UBX_NAV_ID_RELPOSNED: msg_type="relposned"; break;
            case UBX_NAV_ID_EOE: msg_type="eoe"; break;
          }
        }
        break;
      case UBX_ACK_CLASS:
        msg_class = "ack";
        {
          switch (hdr->msg_id) {
            case UBX_ACK_ID_ACK: msg_type="ack"; break;
            case UBX_ACK_ID_NACK: msg_type="nack"; break;
          }
        }
        break;
      case UBX_MON_CLASS:
        msg_class = "mon";
        break;
    }
    ROS_INFO_STREAM("Got message: len=" <<bytes.data.size() <<" payload=" <<hdr->payload_size
                                        <<" s1=" <<static_cast<int>(hdr->sync1)
                                        <<" s2=" <<static_cast<int>(hdr->sync2)
                                        <<" type="  <<msg_class <<"/" <<msg_type
                                        <<std::hex <<std::setw(4) <<std::setfill('0')
                                        <<" checksum:" <<recv_checksum <<"|" <<calc_checksum
                                        <<std::dec);
  }

  void parseUbx(const ds_core_msgs::RawData& bytes) {
    // cast to get header
    const ubx_hdr* hdr = reinterpret_cast<const ubx_hdr*>(bytes.data.data());

    // check the checksum
    const uint16_t calc_checksum = ubx_calc_checksum(bytes.data.data(), hdr->payload_size + sizeof(ubx_hdr));
    if (hdr->payload_size + sizeof(ubx_hdr) + sizeof(uint16_t) > bytes.data.size()) {
      ROS_ERROR_STREAM("Malformed UBX message; length longer than buffer");
      return;
    }
    const uint16_t recv_checksum = *reinterpret_cast<const uint16_t*>(
      bytes.data.data() + hdr->payload_size + sizeof(ubx_hdr));

    if (recv_checksum != calc_checksum) {
      ROS_ERROR_STREAM("UBX Checksum mismatch! Dropping...");
      return;
    }

    //printUbx(bytes);

    const uint8_t* payload = reinterpret_cast<const uint8_t*>(hdr) + sizeof(ubx_hdr);

    switch(hdr->msg_class) {
      case UBX_NAV_CLASS: parseUbxNav(bytes, hdr, payload); break;
      case UBX_ACK_CLASS: parseUbxAck(bytes, hdr, payload); break;
      case UBX_MON_CLASS: parseUbxMon(bytes, hdr, payload); break;
      // do nothing by default
    }
  }

  template <typename T>
  T initMessage(const ds_core_msgs::RawData& bytes, uint32_t time_of_week) {
    T ret;
    ret.ds_header.io_time = bytes.ds_header.io_time;
    ret.header.frame_id = frame_id;
    ret.time_of_week = time_of_week;

    // use the big message timestamp if available
    if (big_msg.time_of_week == time_of_week) {
      ret.header.stamp = big_msg.header.stamp;
    } else {
      ret.header.stamp = ret.ds_header.io_time;
    }

    return ret;
  }

  void parseUbxAck(const ds_core_msgs::RawData& bytes, const ubx_hdr* hdr, const uint8_t* payload) {
    const ubx_ack* ack = reinterpret_cast<const ubx_ack*>(payload);
    switch(hdr->msg_id) {
      case UBX_ACK_ID_ACK:
        ROS_INFO_STREAM("\tUBX ACK " <<static_cast<int>(ack->ack_class)
                                     <<"/" <<static_cast<int>(ack->ack_id));
        break;
      case UBX_ACK_ID_NACK:
        ROS_INFO_STREAM("\tUBX NACK " <<static_cast<int>(ack->ack_class)
                                     <<"/" <<static_cast<int>(ack->ack_id));
        break;
      default:
        ROS_INFO_STREAM("\tUnknown UBX ACK class");
    }
  }

  void parseUbxMon(const ds_core_msgs::RawData& bytes, const ubx_hdr* hdr, const uint8_t* payload) {
    // we dont actually do anything with this
    ROS_INFO_STREAM("\tUBX MON parsing not implemented");
  }

  void parseUbxNav(const ds_core_msgs::RawData& bytes, const ubx_hdr* hdr, const uint8_t* payload) {
    switch (hdr->msg_id) {
      case UBX_NAV_ID_PVT:
        parseUbxNavPVT(bytes, hdr, payload);
        break;
      case UBX_NAV_ID_HPPOSLLH:
        parseUbxNavHPPOSLLH(bytes, hdr, payload);
        break;
      case UBX_NAV_ID_SAT:
        parseUbxNavSAT(bytes, hdr, payload);
        break;
      case UBX_NAV_ID_SIG:
        parseUbxNavSIG(bytes, hdr, payload);
        break;
      case UBX_NAV_ID_SVIN:
        parseUbxNavSVIN(bytes, hdr, payload);
        break;
      case UBX_NAV_ID_RELPOSNED:
        parseUbxNavRELPOSNED(bytes, hdr, payload);
        break;
      case UBX_NAV_ID_EOE:
        parseUbxNavEOE(bytes, hdr, payload);
        break;
    }
  }

  void parseUbxNavPVT(const ds_core_msgs::RawData& bytes, const ubx_hdr* hdr, const uint8_t* payload) {
    const ubx_nav_pvt* pvt = reinterpret_cast<const ubx_nav_pvt*>(payload);

    if (big_msg.time_of_week != pvt->iTimeOfWeek) {
      big_msg = initMessage<ds_sensor_msgs::UbloxNav>(bytes, pvt->iTimeOfWeek);
    }
    big_msg.longitude = 1.0e-7 * ((double)pvt->lon);
    big_msg.latitude = 1.0e-7 * ((double)pvt->lat);
    big_msg.height_ellipsoid = 1.0e-3 * ((double)(pvt->height_ellipsoid));
    big_msg.height_msl = 1.0e-3 * ((double)(pvt->height_msl));
    big_msg.accuracy_horz = 1.0e-3 * ((double)(pvt->accuracy_horz));
    big_msg.accuracy_vert = 1.0e-3 * ((double)(pvt->accuracy_vert));

    big_msg.velocity_north = 1.0e-3 * ((double)(pvt->velocity_north));
    big_msg.velocity_east = 1.0e-3 * ((double)(pvt->velocity_east));
    big_msg.velocity_down = 1.0e-3 * ((double)(pvt->velocity_down));
    big_msg.accuracy_vel = 1.0e-3 * ((double)(pvt->accuracy_speed));
    big_msg.accuracy_time = 1.0e-9 * ((double)(pvt->time_accuracy));

    switch (pvt->fix_type) {
      case 0: big_msg.fixtype = "nofix";
        break;
      case 1: big_msg.fixtype = "dr";
        break;
      case 2: big_msg.fixtype = "gnss2D";
        break;
      case 3: big_msg.fixtype = "gnss3D";
        break;
      case 4: big_msg.fixtype = "gnssdr";
        break;
      case 5: big_msg.fixtype = "time";
        break;
      default:std::stringstream str;
        str << static_cast<int>(pvt->fix_type);
        big_msg.fixtype = str.str();
    }

    // build a timestamp
    boost::posix_time::ptime t( boost::gregorian::date(pvt->year, pvt->month, pvt->day),
        boost::posix_time::time_duration(pvt->hour, pvt->minute, pvt->second));
    big_msg.header.stamp = ros::Time::fromBoost(t) + ros::Duration(static_cast<double>(pvt->nano)*1.0e-9);

    //ROS_INFO_STREAM("\tFix type: " <<big_msg.fixtype <<" " <<big_msg.longitude <<" " <<big_msg.latitude
    //<<" " <<big_msg.height_msl <<"+/-" <<big_msg.accuracy_horz <<"m / " <<big_msg.accuracy_vert <<"m");
  }

  void parseUbxNavHPPOSLLH(const ds_core_msgs::RawData& bytes, const ubx_hdr* hdr, const uint8_t* payload) {
    const ubx_nav_hpposllh* hpposllh = reinterpret_cast<const ubx_nav_hpposllh*>(payload);

    if (big_msg.time_of_week != hpposllh->iTimeOfWeek) {
      big_msg = initMessage<ds_sensor_msgs::UbloxNav>(bytes, hpposllh->iTimeOfWeek);
    }

    big_msg.longitude = 1.0e-7 * (static_cast<double>(hpposllh->lon) + 1.0e-2 * static_cast<double>(hpposllh->high_lon));
    big_msg.latitude = 1.0e-7 * (static_cast<double>(hpposllh->lat) + 1.0e-2 * static_cast<double>(hpposllh->high_lat));
    big_msg.height_ellipsoid = 1.0e-3 * (static_cast<double>(hpposllh->height_ell) + 0.1 * static_cast<double>(hpposllh->high_height_ell));
    big_msg.height_msl = 1.0e-3 * (static_cast<double>(hpposllh->height_msl) + 0.1 * static_cast<double>(hpposllh->high_height_msl));
    big_msg.accuracy_horz = 1.0e-4 * static_cast<double>(hpposllh->accuracy_horz);
    big_msg.accuracy_vert = 1.0e-4 * static_cast<double>(hpposllh->accuracy_vert);
  }

  std::string _parseGnssId(uint8_t gnss_id) {
    switch (gnss_id) {
      case UBX_GNSS_ID_GPS: return "GPS";
      case UBX_GNSS_ID_SBAS: return "SBAS";
      case UBX_GNSS_ID_GALILEO: return "Galileo";
      case UBX_GNSS_ID_BEIDOU: return "BeiDou";
      case UBX_GNSS_ID_QZSS: return "QZSS";
      case UBX_GNSS_ID_GLONASS: return "GLONASS";
      default: return "Unknown";
    }
  }

  std::string _parseSatId(uint8_t gnss_id, uint8_t sv_id) {
    std::stringstream ret;
    switch (gnss_id) {
      case UBX_GNSS_ID_GPS: ret <<"G"; break;
      case UBX_GNSS_ID_SBAS: ret <<"S"; break;
      case UBX_GNSS_ID_GALILEO: ret <<"E"; break;
      case UBX_GNSS_ID_BEIDOU: ret <<"B"; break;
      case UBX_GNSS_ID_QZSS: ret <<"Q"; break;
      case UBX_GNSS_ID_GLONASS: ret <<"R"; break;
      default: ret <<"?";
    }

    ret <<std::setw(2) <<std::setfill('0') <<static_cast<int>(sv_id);

    return ret.str();
  }

  std::string _parseSigId(uint8_t gnss_id, uint8_t sig_id) {
    switch (gnss_id) {
      case UBX_GNSS_ID_GPS:
      {
        switch (sig_id) {
          case 0: return "L1 C/A";
          case 3: return "L2 CL";
          case 4: return "L2 CM";
          default: return "Unknown";
        }
      }
      case UBX_GNSS_ID_SBAS:
      {
          return "Unknown";
      }
      case UBX_GNSS_ID_GALILEO:
      {
        switch (sig_id) {
          case 0: return "E1 C";
          case 1: return "E1 B";
          case 5: return "E5 bI";
          case 6: return "E5 bQ";
          default: return "Unknown";
        }
      }
      case UBX_GNSS_ID_BEIDOU:
      {
        switch (sig_id) {
          case 0: return "B1I D1";
          case 1: return "B1I D2";
          case 2: return "B2I D1";
          case 3: return "B2I D2";
          default: return "Unknown";
        }
      }
      case UBX_GNSS_ID_QZSS:
      {
        switch (sig_id) {
          case 0: return "L1C/A";
          default: return "Unknown";
        }
      }
      case UBX_GNSS_ID_GLONASS:
      {
        switch (sig_id) {
          case 0: return "L1 OF";
          case 2: return "L2 OF";
          default: return "Unknown";
        }
      }
      default:
        return "Unknown";
    }

  }

  void parseUbxNavSAT(const ds_core_msgs::RawData& bytes, const ubx_hdr* hdr, const uint8_t* payload) {
    const ubx_nav_sat *sig = reinterpret_cast<const ubx_nav_sat *>(payload);

    auto sat_msg = initMessage<ds_sensor_msgs::UbloxSatellites>(bytes, sig->iTimeOfWeek);

    const uint8_t* ptr = payload + sizeof(ubx_nav_sig);
    sat_msg.sats.resize(sig->num_signals);
    for (size_t i=0; i<sat_msg.sats.size(); i++) {
      const ubx_nav_sat_entry *s = reinterpret_cast<const ubx_nav_sat_entry *>(ptr);
      ptr += sizeof(ubx_nav_sat_entry);

      sat_msg.sats[i].gnss_id = _parseGnssId(s->gnss_id);
      sat_msg.sats[i].sat_id = _parseSatId(s->gnss_id, s->sv_id);
      sat_msg.sats[i].carrier_noise = s->carrier_noise_ratio;
      sat_msg.sats[i].pseudorange_residual = static_cast<float>(s->pseudorange_residual)*0.1;
      sat_msg.sats[i].elevation = s->elevation;
      sat_msg.sats[i].azimuth = s->azimuth;
      sat_msg.sats[i].quality_indicator = (s->flags & UBX_NAV_SAT_FLAGS_QUALITY);
      sat_msg.sats[i].health = (s->flags & UBX_NAV_SAT_FLAGS_HEALTH) >> 4;
      sat_msg.sats[i].differential_available = static_cast<bool>(s->flags & UBX_NAV_SAT_FLAGS_DIFFCORR);
      sat_msg.sats[i].pseudorange_smoothed = static_cast<bool>(s->flags & UBX_NAV_SAT_FLAGS_PRSMOOTHED);
      sat_msg.sats[i].oribt_source = (s->flags & UBX_NAV_SAT_FLAGS_ORBITSRC) >> 8;

      sat_msg.sats[i].ephAvail = static_cast<bool>(s->flags & UBX_NAV_SAT_FLAGS_EPHMAVAIL);
      sat_msg.sats[i].almAvail = static_cast<bool>(s->flags & UBX_NAV_SAT_FLAGS_ALAMAVAIL);
      sat_msg.sats[i].anoAvail = static_cast<bool>(s->flags & UBX_NAV_SAT_FLAGS_ANOFFAVAIL);
      sat_msg.sats[i].aopAvail = static_cast<bool>(s->flags & UBX_NAV_SAT_FLAGS_ANAUTOAVAIL);

      sat_msg.sats[i].sbasCorrUsed = static_cast<bool>(s->flags & UBX_NAV_SAT_FLAGS_SBASCORRUSED);
      sat_msg.sats[i].rtcmCorrUsed = static_cast<bool>(s->flags & UBX_NAV_SAT_FLAGS_RTCMCORRUSED);
      sat_msg.sats[i].slasCorrUsed = static_cast<bool>(s->flags & UBX_NAV_SAT_FLAGS_SLASCORRUSED);
      sat_msg.sats[i].pseudorange_corrections_applied = static_cast<bool>(s->flags & UBX_NAV_SAT_FLAGS_PRCORRUSED);
      sat_msg.sats[i].carrier_range_corrections_applied = static_cast<bool>(s->flags & UBX_NAV_SAT_FLAGS_CRCORRUSED);
      sat_msg.sats[i].doppler_corrections_applied = static_cast<bool>(s->flags & UBX_NAV_SAT_FLAGS_DOCORRUSED);
    }

    ubx_sat_pub.publish(sat_msg);
  }

  void parseUbxNavSIG(const ds_core_msgs::RawData& bytes, const ubx_hdr* hdr, const uint8_t* payload) {
    const ubx_nav_sig* sig = reinterpret_cast<const ubx_nav_sig*>(payload);

    // fill in headers
    auto sig_msg = initMessage<ds_sensor_msgs::UbloxSignals>(bytes, sig->iTimeOfWeek);

    // fill in message
    const uint8_t* ptr = payload + sizeof(ubx_nav_sig);
    sig_msg.sat_signals.resize(sig->num_signals);
    for (size_t i=0; i<sig_msg.sat_signals.size(); i++) {
      const ubx_nav_sig_entry* s = reinterpret_cast<const ubx_nav_sig_entry*>(ptr);
      ptr += sizeof(ubx_nav_sig_entry);

      // parse the specific IDs
      sig_msg.sat_signals[i].gnss_id = _parseGnssId(s->gnss_id);
      sig_msg.sat_signals[i].sat_id = _parseSatId(s->gnss_id, s->sv_id);
      sig_msg.sat_signals[i].sig_id = _parseSigId(s->gnss_id, s->sig_id);
      sig_msg.sat_signals[i].pseudorange_residual = static_cast<float>(s->pseudorange_residual)*0.1;
      sig_msg.sat_signals[i].carrier_noise = s->carrier_noise_ratio;
      sig_msg.sat_signals[i].quality_indicator = s->quality;
      sig_msg.sat_signals[i].correction_source = s->correction_source;
      sig_msg.sat_signals[i].ionosphere_model = s->ionosphere_model;
      sig_msg.sat_signals[i].health = s->signal_flags & UBX_NAV_SIG_FLAGS_HEALTH;
      sig_msg.sat_signals[i].pseudorange_smoothed = static_cast<bool>(s->signal_flags & UBX_NAV_SIG_FLAGS_PRSMOOTHED);
      sig_msg.sat_signals[i].pseudorange_used = static_cast<bool>(s->signal_flags & UBX_NAV_SIG_FLAGS_PRUSED);
      sig_msg.sat_signals[i].carrier_range_used = static_cast<bool>(s->signal_flags & UBX_NAV_SIG_FLAGS_CRUSED);
      sig_msg.sat_signals[i].doppler_used = static_cast<bool>(s->signal_flags & UBX_NAV_SIG_FLAGS_DOUSED);
      sig_msg.sat_signals[i].pseudorange_corrections_applied = static_cast<bool>(s->signal_flags & UBX_NAV_SIG_FLAGS_PRCORRUSED);
      sig_msg.sat_signals[i].carrier_range_corrections_applied = static_cast<bool>(s->signal_flags & UBX_NAV_SIG_FLAGS_CRCORRUSED);
      sig_msg.sat_signals[i].doppler_corrections_applied = static_cast<bool>(s->signal_flags & UBX_NAV_SIG_FLAGS_DOCORRUSED);
    }

    // publish!
    ubx_sig_pub.publish(sig_msg);
  }

  void parseUbxNavSVIN(const ds_core_msgs::RawData& bytes, const ubx_hdr* hdr, const uint8_t* payload) {
    const ubx_nav_svin* svin = reinterpret_cast<const ubx_nav_svin*>(payload);

    auto svin_msg = initMessage<ds_sensor_msgs::UbloxSurveyIn>(bytes, svin->iTimeOfWeek);
    svin_msg.header.stamp = big_msg.header.stamp;
    svin_msg.ds_header = big_msg.ds_header;
    svin_msg.header.frame_id = frame_id;

    svin_msg.mean_ecef.x = 1.0e-2 * (static_cast<double>(svin->mean_ecef_x)
        + 0.01 * static_cast<double>(svin->mean_high_x));
    svin_msg.mean_ecef.y = 1.0e-2 * (static_cast<double>(svin->mean_ecef_y)
        + 0.01 * static_cast<double>(svin->mean_high_y));
    svin_msg.mean_ecef.z = 1.0e-2 * (static_cast<double>(svin->mean_ecef_z)
        + 0.01 * static_cast<double>(svin->mean_high_z));

    svin_msg.surveyin_accuracy = 1.0e-4 * static_cast<float>(svin->accuracy_mean);
    svin_msg.duration = svin->duration;
    svin_msg.observations = svin->observations;
    svin_msg.valid = svin->valid;
    svin_msg.active = svin->active;

    ubx_svin_pub.publish(svin_msg);
  }

  void parseUbxNavRELPOSNED(const ds_core_msgs::RawData& bytes, const ubx_hdr* hdr, const uint8_t* payload) {
    const ubx_nav_relposned* relposned = reinterpret_cast<const ubx_nav_relposned*>(payload);

    if (big_msg.time_of_week != relposned->iTimeOfWeek) {
      auto big_msg = initMessage<ds_sensor_msgs::UbloxNav>(bytes, relposned->iTimeOfWeek);
    }

    big_msg.relpos_north = 0.01 * (static_cast<double>(relposned->rel_pos_north) + 1.0e-2*static_cast<double>(relposned->high_rel_pos_north));
    big_msg.relpos_east  = 0.01 * (static_cast<double>(relposned->rel_pos_east ) + 1.0e-2*static_cast<double>(relposned->high_rel_pos_east));
    big_msg.relpos_down  = 0.01 * (static_cast<double>(relposned->rel_pos_down ) + 1.0e-2*static_cast<double>(relposned->high_rel_pos_down));
    big_msg.relpos_length  = 0.01 * (static_cast<double>(relposned->rel_pos_length ) + 1.0e-2*static_cast<double>(relposned->high_rel_pos_length));
    big_msg.relpos_heading = 1.0e-5 * static_cast<double>(relposned->rel_pos_heading);

    big_msg.relpos_accuracy_north = 1.0e-4 * static_cast<double>(relposned->accuracy_north);
    big_msg.relpos_accuracy_east  = 1.0e-4 * static_cast<double>(relposned->accuracy_east);
    big_msg.relpos_accuracy_down  = 1.0e-4 * static_cast<double>(relposned->accuracy_down);
    big_msg.relpos_accuracy_length  = 1.0e-4 * static_cast<double>(relposned->accuracy_length);
    big_msg.relpos_accuracy_heading  = 1.0e-5 * static_cast<double>(relposned->accuracy_heading);

    big_msg.relpos_flags = relposned->flags;

    /*
    if (big_msg.relpos_flags & ds_sensor_msgs::UbloxNav::RELPOS_FLAGS_DIFFSOLN) {
      ROS_INFO_STREAM("Differential solution!");
    }
    if (big_msg.relpos_flags & ds_sensor_msgs::UbloxNav::RELPOS_FLAGS_RELPOSVALID) {
        ROS_INFO_STREAM("Relative position VALID!");
    }
    */
  }

  void parseUbxNavEOE(const ds_core_msgs::RawData& bytes, const ubx_hdr* hdr, const uint8_t* payload) {
    const ubx_nav_eoe* eoe = reinterpret_cast<const ubx_nav_eoe*>(payload);

    // publish our native message
    big_msg.header.frame_id = frame_id;
    ubx_nav_pub.publish(big_msg);

    // convert to and publish a navsat fix
    sensor_msgs::NavSatFix navsat;
    navsat.header = big_msg.header;
    navsat.longitude = big_msg.longitude;
    navsat.latitude = big_msg.latitude;
    navsat.altitude = big_msg.height_ellipsoid;
    navsat.position_covariance[0] = big_msg.accuracy_horz * big_msg.accuracy_horz;
    navsat.position_covariance[4] = big_msg.accuracy_horz * big_msg.accuracy_horz;
    navsat.position_covariance[8] = big_msg.accuracy_vert * big_msg.accuracy_vert;
    navsat.position_covariance_type = sensor_msgs::NavSatFix::COVARIANCE_TYPE_DIAGONAL_KNOWN;

    navsat.status.status = sensor_msgs::NavSatStatus::STATUS_NO_FIX;
    if (big_msg.fixtype == "gnss3D") {
      navsat.status.status = sensor_msgs::NavSatStatus::STATUS_FIX;

      // the UBLOX F9P doesn't actually support SBAS; you're supposed to
      // just use RTK corrections

      // note: it's possible for this flag to be set EVEN if the fix is invalid.  Ick.
      if (big_msg.relpos_flags & ds_sensor_msgs::UbloxNav::RELPOS_FLAGS_DIFFSOLN) {
        navsat.status.status = sensor_msgs::NavSatStatus::STATUS_GBAS_FIX;
      }
    }

    navsat_pub.publish(navsat);

    // convert to and publish a point
    geometry_msgs::PointStamped pt;
    pt.header.stamp = big_msg.header.stamp;
    pt.header.frame_id = base_station_id;
    // the point will expect east/north/up
    pt.point.x = big_msg.relpos_east;
    pt.point.y = big_msg.relpos_north;
    pt.point.z = -big_msg.relpos_down;

    rover_point.publish(pt);
  }

};

} // namespace ds_sensors

#endif //DS_SENSORS_UBLOX_RTK_PRIVATE_H_
