//
// Created by ivaughn on 8/17/19.
//

#ifndef RTKBASE_SRC_UBX_H_
#define RTKBASE_SRC_UBX_H_

#include <stddef.h>
#include <stdint.h>

#define UBX_SYNC1_SIGNED -75
#define UBX_SYNC1 0xb5
#define UBX_SYNC2 0x62

struct ubx_hdr {
  uint8_t sync1;
  uint8_t sync2;
  uint8_t msg_class;
  uint8_t msg_id;
  uint16_t payload_size; // little-endian
} __attribute__((packed));
#define UBX_CHECKSUM_LEN 2

// this is the header for the config get/set command
#define UBX_CFG_VERSION 0x01
#define UBX_CFG_LAYER_RAM 0x01
#define UBX_CFG_LAYER_BBR 0x02
#define UBX_CFG_LAYER_FLASH 0x04
#define UBX_CFG_LAYER_DEFAULT 0x07

#define UBX_L_ENABLE 0x01
#define UBX_L_DISABLE 0x00

struct ubx_cfg_hdr {
  uint8_t version; // 1
  uint8_t layer;   // see above
  uint8_t transaction;
  uint8_t reserved;
} __attribute__((packed));

struct ubx_reset_cmd {
  struct ubx_hdr hdr;
  uint16_t navBbrMask;
  uint8_t resetMode;
  uint8_t reserved1;
  uint16_t chksum;
} __attribute__((packed));

#define UBX_RESET_MASK_STARTHOT 0x0000
#define UBX_RESET_MASK_STARTWARM 0x0001
#define UBX_RESET_MASK_STARTCOLD 0xffff
#define UBX_RESET_MASK_EPH 0x0001
#define UBX_RESET_MASK_ALM 0x0002
#define UBX_RESET_MASK_HEALTH 0x0004
#define UBX_RESET_MASK_KLOB 0x0008
#define UBX_RESET_MASK_POS 0x0010
#define UBX_RESET_MASK_CLKD 0x0020
#define UBX_RESET_MASK_OSC 0x0040
#define UBX_RESET_MASK_UTC 0x0080
#define UBX_RESET_MASK_RTC 0x0100
#define UBX_RESET_MASK_AOP 0x8000

#define UBX_RESET_MODE_HARDWARE 0x00
#define UBX_RESET_MODE_SOFTWARE 0x01
#define UBX_RESET_MODE_SOFTWARE_GNSS_ONLY 0x02
#define UBX_RESET_MODE_HARDWARE_SHUTDOWN 0x04
#define UBX_RESET_MODE_GNSS_STOP 0x08
#define UBX_RESET_MODE_GNSS_START 0x09


// offset from base for each option to the desired port
#define UBX_OUT_I2C   0x0
#define UBX_OUT_UART1 0x1
#define UBX_OUT_UART2 0x2
#define UBX_OUT_USB   0x3
#define UBX_OUT_SPI   0x4

// basic I/O config

#define UBX_CFG_UART1_BASE          0x10730000
#define UBX_CFG_UART2_BASE          0x10750000
#define UBX_CFG_USB_BASE            0x10770000

#define INPROTO_UBX   0x00000001
#define INPROTO_NMEA  0x00000002
#define INPROTO_RTCM  0x00000004
#define OUTPROTO_UBX  0x00010001
#define OUTPROTO_NMEA 0x00010002
#define OUTPROTO_RTCM 0x00010004

// Standard NMEA Output Messages
#define UBX_OUT_NMEA_DTM   0x209100a6
#define UBX_OUT_NMEA_GBS   0x209100dd
#define UBX_OUT_NMEA_GGA   0x209100ba
#define UBX_OUT_NMEA_GLL   0x209100c9
#define UBX_OUT_NMEA_GNS   0x209100b5
#define UBX_OUT_NMEA_GRS   0x209100ce
#define UBX_OUT_NMEA_GSA   0x209100bf
#define UBX_OUT_NMEA_GST   0x209100d3
#define UBX_OUT_NMEA_GSV   0x209100c4
#define UBX_OUT_NMEA_RMC   0x209100ab
#define UBX_OUT_NMEA_VLW   0x209100e7
#define UBX_OUT_NMEA_VTG   0x209100b0
#define UBX_OUT_NMEA_ZDA   0x209100d8

// there are TONS more less-standard NMEA messagse.  But its NMEA, so who cares?

// RTCM-3X messages
#define UBX_OUT_RTCM_TYPE1005 0x209102bd
#define UBX_OUT_RTCM_TYPE1074 0x2091035e
#define UBX_OUT_RTCM_TYPE1077 0x209102cc
#define UBX_OUT_RTCM_TYPE1084 0x20910363
#define UBX_OUT_RTCM_TYPE1087 0x209102d1
#define UBX_OUT_RTCM_TYPE1094 0x20910368
#define UBX_OUT_RTCM_TYPE1097 0x20910318
#define UBX_OUT_RTCM_TYPE1124 0x2091036d
#define UBX_OUT_RTCM_TYPE1127 0x209102d6
#define UBX_OUT_RTCM_TYPE1230 0x20910303

// UBX messages
#define UBX_OUT_UBX_LOG           0x20910259
#define UBX_OUT_UBX_MON_COMMS     0x2091034F
#define UBX_OUT_UBX_MON_HW        0x209101b4
#define UBX_OUT_UBX_MON_HW2       0x209101b9
#define UBX_OUT_UBX_MON_HW3       0x20910354
#define UBX_OUT_UBX_MON_IO        0x209101a5
#define UBX_OUT_UBX_MON_MSGPP     0x20910196
#define UBX_OUT_UBX_MON_RF        0x20910359
#define UBX_OUT_UBX_MON_RXBUF     0x20910187
#define UBX_OUT_UBX_MON_RXR       0x209101a5
#define UBX_OUT_UBX_MON_TXBUF     0x2091019b

#define UBX_OUT_UBX_NAV_CLOCK     0x20910065
#define UBX_OUT_UBX_NAV_DOP       0x20910038
#define UBX_OUT_UBX_NAV_EOE       0x2091015f
#define UBX_OUT_UBX_NAV_GEOFENCE  0x209100a1
#define UBX_OUT_UBX_NAV_HPPOSECEF 0x2091002e
#define UBX_OUT_UBX_NAV_HPPOSLLH  0x20910033
#define UBX_OUT_UBX_NAV_ODO       0x2091007e
#define UBX_OUT_UBX_NAV_ORB       0x20910010
#define UBX_OUT_UBX_NAV_POSECEF   0x20910024
#define UBX_OUT_UBX_NAV_POSLLH    0x20910029
#define UBX_OUT_UBX_NAV_PVT       0x20910006
#define UBX_OUT_UBX_NAV_RELPOSNED 0x2091008d
#define UBX_OUT_UBX_NAV_SAT       0x20910015
#define UBX_OUT_UBX_NAV_SIG       0x20910345
#define UBX_OUT_UBX_NAV_STATUS    0x2091001a
#define UBX_OUT_UBX_NAV_SVIN      0x20910088
#define UBX_OUT_UBX_NAV_TIMEBDS   0x20910051
#define UBX_OUT_UBX_NAV_TIMEGAL   0x20910056
#define UBX_OUT_UBX_NAV_TIMEGLO   0x2091004c
#define UBX_OUT_UBX_NAV_TIMEGPS   0x20910047
#define UBX_OUT_UBX_NAV_TIMELS    0x20910060
#define UBX_OUT_UBX_NAV_TIMEUTC   0x2091005b
#define UBX_OUT_UBX_NAV_VELECEF   0x2091003d
#define UBX_OUT_UBX_NAV_VELNED    0x20910042

#define UBX_OUT_UBX_RXM_MEASX 0x20910204
#define UBX_OUT_UBX_RXM_RAWX  0x209102a4
#define UBX_OUT_UBX_RXM_RLM   0x2091025e
#define UBX_OUT_UBX_RXM_RTCM  0x20910268
#define UBX_OUT_UBX_RXM_SFRBX 0x20910231

#define UBX_OUT_UBX_TIM_TM2   0x20910178
#define UBX_OUT_UBX_TIM_TP    0x2091017d
#define UBX_OUT_UBX_TIM_VRFY  0x20910092

#define UBX_CFG_TMODE         0x20030001
#define UBX_CFG_TMODE_DISABLED  0x00
#define UBX_CFG_TMODE_SURVEY_IN 0x01
#define UBX_CFG_TMODE_FIXED     0x02
#define UBX_CFG_TMODE_SVIN_MIN_DUR   0x40030010
#define UBX_CFG_TMODE_SVIN_ACC_LIMIT 0x40030011

#define UBX_CFG_RATE_MEAS 0x30210001
#define UBX_CFG_RATE_NAV 0x30210002
#define UBX_CFG_RATE_TIMEREF 0x20210003

#define UBX_NAV_PVT_FIXTYPE_NOFIX  0
#define UBX_NAV_PVT_FIXTYPE_DR     1
#define UBX_NAV_PVT_FIXTYPE_2D     2
#define UBX_NAV_PVT_FIXTYPE_3D     3
#define UBX_NAV_PVT_FIXTYPE_GNSSDR 4
#define UBX_NAV_PVT_FIXTYPE_TIME   5

#define UBX_NAV_CLASS 0x01
#define UBX_NAV_ID_PVT 0x07
#define UBX_NAV_ID_HPPOSLLH 0x14
#define UBX_NAV_ID_SAT 0x35
#define UBX_NAV_ID_SIG 0x43
#define UBX_NAV_ID_SVIN 0x3B
#define UBX_NAV_ID_RELPOSNED 0x3C
#define UBX_NAV_ID_EOE 0x61


#define UBX_MON_CLASS 0x0a

#define UBX_ACK_CLASS 0x05
#define UBX_ACK_ID_ACK 0x01
#define UBX_ACK_ID_NACK 0x00

struct ubx_ack {
  uint8_t ack_class;
  uint8_t ack_id;
} __attribute__((packed));

#define UBX_CFG_CLASS 0x06
#define UBX_CFG_ID_RESET 0x04
#define UBX_CFG_ID_VALGET 0x8b
#define UBX_CFG_ID_VALSET 0x8a

struct ubx_nav_hpposllh {
  uint8_t version;
  uint8_t reserved[2];
  uint8_t flags_addl;
  uint32_t iTimeOfWeek; // ms
  int32_t lon; // 1.0e-7 deg
  int32_t lat; // 1.0e-7 deg
  int32_t height_ell;       // mm, height above ellipsoid
  int32_t height_msl;       // mm, height above mean sea level
  int8_t high_lon;         // 0.1 mm, high-accuracy component of longitude
  int8_t high_lat;         // 0.1 mm, high-accuracy component of latitude
  int8_t high_height_ell;  // 0.1 mm, high-accuracy component of height over ellipsoid
  int8_t high_height_msl;  // 0.1 mm, high-accuracy component of height over mean sea level
  uint32_t accuracy_horz; // mm, horizontal accuracy
  uint32_t accuracy_vert; // mm, vertical accuracy
} __attribute__((packed));

#define UBX_NAV_PVT_VALID_DATE 0x01
#define UBX_NAV_PVT_VALID_TIME 0x02
#define UBX_NAV_PVT_VALID_FULL 0x04
#define UBX_NAV_PVT_VALID_MAG  0x08

struct ubx_nav_pvt {
  uint32_t iTimeOfWeek; // ms
  uint16_t year;
  uint8_t month;
  uint8_t day;
  uint8_t hour;
  uint8_t minute;
  uint8_t second;
  uint8_t valid;
  uint32_t time_accuracy; // ns
  int32_t nano; // ns
  uint8_t fix_type;
  uint8_t fix_status_flags;
  uint8_t fix_addl_flags;
  uint8_t num_sv; // number of satellites used
  int32_t lon; // 1e-7 degrees
  int32_t lat; // 1e-7 degrees
  int32_t height_ellipsoid; // mm, height above ellipsoid
  int32_t height_msl; // mm, height above mean sealevel
  uint32_t accuracy_horz; // mm, horizontal accuracy
  uint32_t accuracy_vert; // mm, vertical accuracy
  int32_t velocity_north; // mm/s
  int32_t velocity_east; // mm/s
  int32_t velocity_down; // mm/s
  int32_t ground_speed; //mm/s
  int32_t heading_motion; // course.  1e-5 deg
  uint32_t accuracy_speed; // mm/s
  uint32_t accuracy_heading; // 1e-5 deg
  uint16_t position_dop; // 0.01 (scaling).  Position dilution of precision
  uint8_t flags_addl;
  uint8_t reserved[5];
  int32_t vehicle_heading; // heading of vehicle, 1e-5 deg
  int16_t magnetic_declination; // 1e-2 deg
  uint16_t accuracy_magnetic_declination; // 1e-2 deg
} __attribute__((packed));

struct ubx_nav_relposned {
  uint8_t version;
  uint8_t reserved1;
  uint16_t ref_station_id;
  uint32_t iTimeOfWeek; // ms
  int32_t rel_pos_north; // cm
  int32_t rel_pos_east;  // cm
  int32_t rel_pos_down;  // cm
  int32_t rel_pos_length; // cm; length of relative position vector
  int32_t rel_pos_heading; // 1.0e-5 deg; heading of relative position vector
  uint8_t reserved2[4];
  int8_t high_rel_pos_north; // add to rel_pos.  0.1mm.
  int8_t high_rel_pos_east;  // add to rel_pos.  0.1mm.
  int8_t high_rel_pos_down;  // add to rel_pos.  0.1mm.
  int8_t high_rel_pos_length; // add to rel_pos.  0.1mm.
  uint32_t accuracy_north; // 0.1 mm
  uint32_t accuracy_east; // 0.1 mm
  uint32_t accuracy_down; // 0.1 mm
  uint32_t accuracy_length; // 0.1 mm
  uint32_t accuracy_heading; // 1.0e-5 deg
  uint8_t reserved3[4];
  uint32_t flags;
} __attribute__((packed));

#define UBX_NAV_SIG_QUALITY_NOSIG           0
#define UBX_NAV_SIG_QUALITY_SEARCH          1
#define UBX_NAV_SIG_QUALITY_ACQUIRED        2
#define UBX_NAV_SIG_QUALITY_UNUSABLE        3
#define UBX_NAV_SIG_QUALITY_CODE_LOCKED     4
#define UBX_NAV_SIG_QUALITY_CARRIER_LOCKED1 5
#define UBX_NAV_SIG_QUALITY_CARRIER_LOCKED2 6
#define UBX_NAV_SIG_QUALITY_CARRIER_LOCKED3 7

#define UBX_NAV_SIG_CORR_NONE          0
#define UBX_NAV_SIG_CORR_SBAS          1
#define UBX_NAV_SIG_CORR_BEIDOU        2
#define UBX_NAV_SIG_CORR_RTCM2         3
#define UBX_NAV_SIG_CORR_RTCM3_OSR     4
#define UBX_NAV_SIG_CORR_RTCM3_SSR     5
#define UBX_NAV_SIG_CORR_QZSS_SLAS     6

#define UBX_NAV_SIG_IONO_NONE          0
#define UBX_NAV_SIG_IONO_KLOBUCHAR_GPS 1
#define UBX_NAV_SIG_IONO_SBAS          2
#define UBX_NAV_SIG_IONO_KLOBUCHAR_BDU 3

#define UBX_NAV_SAT_FLAGS_QUALITY 0x0003
#define UBX_NAV_SAT_FLAGS_SVUSED 0x0004
#define UBX_NAV_SAT_FLAGS_HEALTH 0x0030
#define UBX_NAV_SAT_FLAGS_DIFFCORR 0x0040
#define UBX_NAV_SAT_FLAGS_PRSMOOTHED 0x0080
#define UBX_NAV_SAT_FLAGS_ORBITSRC 0x0700
#define UBX_NAV_SAT_FLAGS_EPHMAVAIL 0x0800
#define UBX_NAV_SAT_FLAGS_ALAMAVAIL 0x1000
#define UBX_NAV_SAT_FLAGS_ANOFFAVAIL 0x2000
#define UBX_NAV_SAT_FLAGS_ANAUTOAVAIL 0x4000
#define UBX_NAV_SAT_FLAGS_SBASCORRUSED 0x010000
#define UBX_NAV_SAT_FLAGS_RTCMCORRUSED 0x020000
#define UBX_NAV_SAT_FLAGS_SLASCORRUSED 0x040000
#define UBX_NAV_SAT_FLAGS_PRCORRUSED 0x100000
#define UBX_NAV_SAT_FLAGS_CRCORRUSED 0x200000
#define UBX_NAV_SAT_FLAGS_DOCORRUSED 0x400000

#define UBX_NAV_SIG_FLAGS_HEALTH 0x0003
#define UBX_NAV_SIG_FLAGS_PRSMOOTHED 0x0004
#define UBX_NAV_SIG_FLAGS_PRUSED 0x0008
#define UBX_NAV_SIG_FLAGS_CRUSED 0x0010
#define UBX_NAV_SIG_FLAGS_DOUSED 0x0020
#define UBX_NAV_SIG_FLAGS_PRCORRUSED 0x0040
#define UBX_NAV_SIG_FLAGS_CRCORRUSED 0x0080
#define UBX_NAV_SIG_FLAGS_DOCORRUSED 0x0100

#define UBX_GNSS_ID_GPS 0
#define UBX_GNSS_ID_SBAS 1
#define UBX_GNSS_ID_GALILEO 2
#define UBX_GNSS_ID_BEIDOU 3
#define UBX_GNSS_ID_QZSS 5
#define UBX_GNSS_ID_GLONASS 6

struct ubx_nav_sig_entry {
  uint8_t gnss_id;
  uint8_t sv_id;
  uint8_t sig_id;
  uint8_t freq_id;
  int16_t pseudorange_residual; // 0.1 m
  uint8_t carrier_noise_ratio; //dBHz
  uint8_t quality;
  uint8_t correction_source;
  uint8_t ionosphere_model;
  uint16_t signal_flags;
  uint8_t reserved[4];
} __attribute__((packed));

struct ubx_nav_sig {
  uint32_t iTimeOfWeek; // ms
  uint8_t version;
  uint8_t num_signals;
  uint16_t reserved1;

  // now a bunch of signal structs
} __attribute__((packed));

struct ubx_nav_sat_entry {
  uint8_t gnss_id;
  uint8_t sv_id;
  uint8_t carrier_noise_ratio; //dBHz
  int8_t elevation; // degrees
  int16_t azimuth; // degrees
  int16_t pseudorange_residual; // 0.1 m
  uint32_t flags;
} __attribute__((packed));

struct ubx_nav_sat {
  uint32_t iTimeOfWeek; // ms
  uint8_t version;
  uint8_t num_signals;
  uint16_t reserved1;

  // now a bunch of satellite structs
} __attribute__((packed));


struct ubx_nav_svin {
  uint8_t version;
  uint8_t reserved[3];
  uint32_t iTimeOfWeek; // ms
  uint32_t duration; // seconds
  int32_t mean_ecef_x; // cm
  int32_t mean_ecef_y; // cm
  int32_t mean_ecef_z; // cm
  int8_t mean_high_x; // 0.1 mm.  High-precision component.
  int8_t mean_high_y; // 0.1 mm.  High-precision component.
  int8_t mean_high_z; // 0.1 mm.  High-precision component.
  uint8_t reserved2;
  uint32_t accuracy_mean; // 0.1 mm
  uint32_t observations;
  uint8_t valid; // 1 = valid, 0 otherwise
  uint8_t active; // survey-in in progress
  uint16_t reserved3;
} __attribute__((packed));

struct ubx_nav_eoe {

} __attribute__((packed));

extern uint16_t ubx_calc_checksum(const uint8_t *const buf, size_t len);

#endif // RTKBASE_SRC_UBX_H_
