//
// Created by ivaughn on 8/18/19.
//

#include "ublox_rtk_private.h"
#include "ublox_matcher.h"

#include <sensor_msgs/NavSatFix.h>
#include <geometry_msgs/PointStamped.h>

#include "ubx.h"
#include "rtcm.h"
#include "ublox_config.h"

namespace ds_sensors {

UbloxGps::UbloxGps() : ds_base::SensorBase(), d_ptr_(new UbloxGpsPrivate()) {
  // do nothing
}

UbloxGps:: UbloxGps(int argc, char* argv[], const std::string& name)
: ds_base::SensorBase(argc, argv, name), d_ptr_(new UbloxGpsPrivate()) {
  // do nothing
}

UbloxGps::~UbloxGps()  = default;

void UbloxGps::setup() {
  // run normal setup
  ds_base::SensorBase::setup();

  // configure the UBlox's output config (only in RAM)
  DS_D(UbloxGps);
  ROS_INFO_STREAM("Sending output config to ublox");
  sendOutputConfig();

  ROS_INFO_STREAM("rover: " <<d->rover);
  _setRover(d->rover);
}

void UbloxGps::setupConnections() {
  SensorBase::setupConnections();
  DS_D(UbloxGps);

  // so the UBlox supports three different protocols (NMEA, RTCM, and their own custom thing).
  // Packetizing serial traffic is a disaster, so just do the fully-custom thing
  auto conn = dynamic_cast<ds_asio::DsSerial*>(connection("instrument"));
  if (conn) {
    conn->set_matcher(ublox_matcher());
  }

  // add connection to UDP multicast for RTCM
  d->corrections = addConnection("corrections", boost::bind(&UbloxGps::_onCorrectionMsg, this, _1));
  // we only really need to packetize RTCM stuff if its coming in over a serial connection--
  // so its probably fine-- but that's included in the full ublox_matcher
  auto corr = boost::dynamic_pointer_cast<ds_asio::DsSerial>(d->corrections);
  if (corr) {
    corr->set_matcher(ublox_matcher());
  }

}

void UbloxGps::setupPublishers() {
  DS_D(UbloxGps);
  auto nh = nodeHandle();

  d->ubx_nav_pub = nh.advertise<ds_sensor_msgs::UbloxNav>(ros::this_node::getName() + "/ublox_nav", 10, false);
  d->ubx_sat_pub = nh.advertise<ds_sensor_msgs::UbloxSatellites>(ros::this_node::getName() + "/ublox_sat", 10, false);
  d->ubx_sig_pub = nh.advertise<ds_sensor_msgs::UbloxSignals>(ros::this_node::getName() + "/ublox_sig", 10, false);
  d->ubx_svin_pub = nh.advertise<ds_sensor_msgs::UbloxSurveyIn>(ros::this_node::getName() + "/ublox_surveyin", 10, false);
  d->navsat_pub = nh.advertise<sensor_msgs::NavSatFix>(ros::this_node::getName() + "/navsat", 10, false);
  d->rover_point = nh.advertise<geometry_msgs::PointStamped>(ros::this_node::getName() + "/rover", 10, false);
}

void UbloxGps::setupParameters() {
  DS_D(UbloxGps);
  auto nh = nodeHandle();

  ros::param::param<std::string>("~frame_id", d->frame_id, "gps_link");
  ros::param::param<std::string>("~basestation_frame", d->base_station_id, "rtk_base_link");
  ros::param::param<bool>("~rover", d->rover, true);
  ROS_INFO_STREAM("rover: " <<d->rover);

  // host connection options
  std::string host_port_name;
  ros::param::param<std::string>("~host_port", host_port_name, "usb");
  if (host_port_name == "usb") {
    d->host_port = UBX_OUT_USB;
    ROS_INFO_STREAM("Using USB output port...");
  } else if (host_port_name == "i2c") {
    d->host_port = UBX_OUT_I2C;
    ROS_INFO_STREAM("Using I2C output port...");
  } else if (host_port_name == "uart1") {
    d->host_port = UBX_OUT_UART1;
    ROS_INFO_STREAM("Using UART1 output port...");
  } else if (host_port_name == "uart2") {
    d->host_port = UBX_OUT_UART2;
    ROS_INFO_STREAM("Using UART2 output port...");
  } else if (host_port_name == "spi") {
    d->host_port = UBX_OUT_SPI;
    ROS_INFO_STREAM("Using SPI output port...");
  } else {
    ROS_FATAL_STREAM("Invalid hostport specified! Use usb|i2c|uart1|uart2|spi, not "
    <<host_port_name);
  }

  float update_hz;
  ros::param::param<float>("~update_hz", update_hz, 1.0);

  d->base_measurement_interval = round(1000.0/update_hz);

  int tmp;
  ros::param::param<int>("~update_downsample", tmp, 1);
  d->update_downsample = tmp;
  ros::param::param<int>("~sat_downsample", tmp, 1);
  d->sat_downsample = tmp;
  ros::param::param<int>("~rtcm_downsample", tmp, 1);
  d->rtcm_downsample = tmp;
  ros::param::param<int>("~port2_nmea_downsample", tmp, 1);
  d->port2_nmea_downsample = tmp;
  ros::param::param<float>("~svin_duration", d->svin_duration, 1);
  ros::param::param<float>("~svin_accuracy", d->svin_accuracy, 1);

  ros::param::param<bool>("~port2_rtcm_disable", d->port2_rtcm_disable, true);

  std::string config_str;
  ros::param::param<std::string>("~config_layer", config_str, "ram");

  if (config_str == "ram") {
    // RAM-- temporary, gets overwritten by removing power
    ROS_INFO_STREAM("Writing UBLOX config to temporary RAM");
    d->config_layer = UBX_CFG_LAYER_RAM;
  } else if (config_str == "bbr") {
    // Battery-Backed RAM-- somewhat persistant, overwritten by resets
    ROS_INFO_STREAM("Writing UBLOX config to battery-backed RAM");
    d->config_layer = UBX_CFG_LAYER_BBR;
  } else if (config_str == "flash") {
    ROS_INFO_STREAM("Writing UBLOX config to FLASH");
    d->config_layer = UBX_CFG_LAYER_FLASH;
  } else if (config_str == "none") {
    ROS_INFO_STREAM("NOT writing UBLOX config (hope you saved a sensible default!");
    d->config_layer = 0;
  } else {
    ROS_ERROR_STREAM("UNKNOWN UBLOX Config target \"" << config_str
                                                      <<"\" not in [ram|bbr|flash|none], falling back on \"ram\"");
    d->config_layer = UBX_CFG_LAYER_RAM;
  }

}

void UbloxGps::setupServices() {
  DS_D(UbloxGps);
  auto nh = nodeHandle();

  // set as base station (or not)
  d->mode_service = nh.advertiseService("set_mode", &UbloxGps::_setMode, this);
}

void UbloxGps::parseReceivedBytes(const ds_core_msgs::RawData& bytes) {
  DS_D(UbloxGps);

  // we have no data, just move on
  if (bytes.data.size() < 1) {
    ROS_ERROR_STREAM("Got zero byte messages!");
    return;
  }

  if (bytes.data[0] == '$') {
    //ROS_INFO_STREAM("CLIENT Got NMEA, len=" <<bytes.data.size());
    // nobody likes NMEA...
    std::string nmea(bytes.data.begin(), bytes.data.end());
    ROS_INFO_STREAM("NMEA: " <<nmea.substr(0, nmea.length()-2));

  } else if (bytes.data[0] == RTCM_SYNC) {
    //ROS_INFO_STREAM("CLIENT Got RTCM, len=" <<bytes.data.size());
    _handleRtcm(bytes);

  } else if (bytes.data[0] == UBX_SYNC1) {
    //ROS_INFO_STREAM("CLIENT Got UBX, len=" <<bytes.data.size());
    d->parseUbx(bytes);
  }
}

void UbloxGps::sendOutputConfig() {
  DS_D(UbloxGps);

  if (d->config_layer == 0) {
    ROS_INFO_STREAM("UBLOX configured to NOT write any output config.  Skipping...");
    return;
  }

  ublox::UbloxConfig config(d->config_layer);

  // disable RTCM on port2
  if (d->port2_rtcm_disable) {
    config.addConfigValue<uint8_t>(UBX_OUT_RTCM_TYPE1005 + UBX_OUT_UART2, 0);
    config.addConfigValue<uint8_t>(UBX_OUT_RTCM_TYPE1074 + UBX_OUT_UART2, 0);
    config.addConfigValue<uint8_t>(UBX_OUT_RTCM_TYPE1077 + UBX_OUT_UART2, 0);
    config.addConfigValue<uint8_t>(UBX_OUT_RTCM_TYPE1084 + UBX_OUT_UART2, 0);
    config.addConfigValue<uint8_t>(UBX_OUT_RTCM_TYPE1087 + UBX_OUT_UART2, 0);
    config.addConfigValue<uint8_t>(UBX_OUT_RTCM_TYPE1094 + UBX_OUT_UART2, 0);
    config.addConfigValue<uint8_t>(UBX_OUT_RTCM_TYPE1097 + UBX_OUT_UART2, 0);
    config.addConfigValue<uint8_t>(UBX_OUT_RTCM_TYPE1124 + UBX_OUT_UART2, 0);
    config.addConfigValue<uint8_t>(UBX_OUT_RTCM_TYPE1127 + UBX_OUT_UART2, 0);
    config.addConfigValue<uint8_t>(UBX_OUT_RTCM_TYPE1230 + UBX_OUT_UART2, 0);
  }

  // enable NMEA, if required

  if (d->port2_nmea_downsample != 0) {
    // type: L
    config.addConfigValue<uint8_t>(OUTPROTO_NMEA + UBX_CFG_UART2_BASE, 1);
  }
  config.addConfigValue<uint8_t>(UBX_OUT_NMEA_GGA + UBX_OUT_UART2, d->port2_nmea_downsample);
  config.addConfigValue<uint8_t>(UBX_OUT_NMEA_GLL + UBX_OUT_UART2, d->port2_nmea_downsample);
  config.addConfigValue<uint8_t>(UBX_OUT_NMEA_GST + UBX_OUT_UART2, d->port2_nmea_downsample);
  config.addConfigValue<uint8_t>(UBX_OUT_NMEA_GSA + UBX_OUT_UART2, 0);
  config.addConfigValue<uint8_t>(UBX_OUT_NMEA_GSV + UBX_OUT_UART2, 0);
  config.addConfigValue<uint8_t>(UBX_OUT_NMEA_RMC + UBX_OUT_UART2, d->port2_nmea_downsample);
  config.addConfigValue<uint8_t>(UBX_OUT_NMEA_ZDA + UBX_OUT_UART2, d->port2_nmea_downsample);

  float update_rate = 1000.0/static_cast<float>(d->base_measurement_interval) / d->update_downsample;
  float nmea_rate = 1000.0/static_cast<float>(d->base_measurement_interval) / d->port2_nmea_downsample;
  float rtcm_rate = 1000.0/static_cast<float>(d->base_measurement_interval) / d->rtcm_downsample;
  float sat_rate = 1000.0/static_cast<float>(d->base_measurement_interval) / d->sat_downsample;

  ROS_INFO_STREAM("HOST PORT: " <<std::hex <<d->host_port);
  ROS_INFO_STREAM("    base interval (ms): " <<std::dec <<static_cast<int>(d->base_measurement_interval));
  ROS_INFO_STREAM("    update downsample: " <<std::dec <<static_cast<int>(d->update_downsample));
  ROS_INFO_STREAM("        --> update rate: " <<std::dec <<update_rate);
  ROS_INFO_STREAM("    sat downsample: " <<std::dec <<static_cast<int>(d->sat_downsample));
  ROS_INFO_STREAM("        --> update rate: " <<std::dec <<sat_rate);
  ROS_INFO_STREAM("    NMEA port update downsample: " <<std::dec <<static_cast<int>(d->port2_nmea_downsample));
  ROS_INFO_STREAM("        --> update rate: " <<std::dec <<nmea_rate);
  ROS_INFO_STREAM("    RTCM port update downsample: " <<std::dec <<static_cast<int>(d->rtcm_downsample));
  ROS_INFO_STREAM("        --> update rate: " <<std::dec <<rtcm_rate);

  // disable NMEA on port 1
  if (d->host_port != UBX_OUT_UART1) {
    config.addConfigValue<uint8_t>(UBX_OUT_NMEA_GGA + UBX_OUT_UART1, 0);
    config.addConfigValue<uint8_t>(UBX_OUT_NMEA_GLL + UBX_OUT_UART1, 0);
    config.addConfigValue<uint8_t>(UBX_OUT_NMEA_GST + UBX_OUT_UART1, 0);
    config.addConfigValue<uint8_t>(UBX_OUT_NMEA_GSA + UBX_OUT_UART1, 0);
    config.addConfigValue<uint8_t>(UBX_OUT_NMEA_GSV + UBX_OUT_UART1, 0);
    config.addConfigValue<uint8_t>(UBX_OUT_NMEA_RMC + UBX_OUT_UART1, 0);
    config.addConfigValue<uint8_t>(UBX_OUT_NMEA_VTG + UBX_OUT_UART1, 0);
    config.addConfigValue<uint8_t>(UBX_OUT_NMEA_ZDA + UBX_OUT_UART1, 0);
  }

  // disable NMEA on USB; there's better stuff in the binary
  config.addConfigValue<uint8_t>(UBX_OUT_NMEA_GGA + d->host_port, 0);
  config.addConfigValue<uint8_t>(UBX_OUT_NMEA_GLL + d->host_port, 0);
  config.addConfigValue<uint8_t>(UBX_OUT_NMEA_GST + d->host_port, 0);
  config.addConfigValue<uint8_t>(UBX_OUT_NMEA_GSA + d->host_port, 0);
  config.addConfigValue<uint8_t>(UBX_OUT_NMEA_GSV + d->host_port, 0);
  config.addConfigValue<uint8_t>(UBX_OUT_NMEA_RMC + d->host_port, 0);
  config.addConfigValue<uint8_t>(UBX_OUT_NMEA_VTG + d->host_port, 0);
  config.addConfigValue<uint8_t>(UBX_OUT_NMEA_ZDA + d->host_port, 0);

  // enable our chosen messages
  config.addConfigValue<uint8_t>(UBX_OUT_UBX_NAV_SAT + d->host_port, d->sat_downsample); // signals
  config.addConfigValue<uint8_t>(UBX_OUT_UBX_NAV_SIG + d->host_port, d->sat_downsample); // signals
  config.addConfigValue<uint8_t>(UBX_OUT_UBX_NAV_SVIN+ d->host_port, d->sat_downsample); // survey-in status
  config.addConfigValue<uint8_t>(UBX_OUT_UBX_NAV_PVT + d->host_port, d->update_downsample); // position, velocity, time
  config.addConfigValue<uint8_t>(UBX_OUT_UBX_NAV_HPPOSLLH + d->host_port, d->update_downsample); // high-res latlon
  config.addConfigValue<uint8_t>(UBX_OUT_UBX_NAV_RELPOSNED + d->host_port, d->update_downsample); // relative position
  config.addConfigValue<uint8_t>(UBX_OUT_UBX_NAV_EOE + d->host_port, d->update_downsample); // end-of-epoch

  config.addConfigValue<uint16_t>(UBX_CFG_RATE_MEAS, d->base_measurement_interval);

  ROS_INFO_STREAM("Sending message with " <<config.keys()
  <<" keys and " <<config.size() <<" bytes!");

  // fill in length, checksum, etc
  connection("instrument")->send(config.toMessage());
}

void UbloxGps::_onCorrectionMsg(ds_core_msgs::RawData in) {

  DS_D(UbloxGps);
  if (d->rover) {
    //ROS_INFO_STREAM("RTCM: passing on " <<in.data.size() <<" bytes!");

    // update the time of last correction
    d->latest_correction_time = in.ds_header.io_time;

    // send out to the instrument
    connection("instrument")->send(std::string(in.data.begin(), in.data.end()));
  } else {
    ROS_WARN_ONCE("Received RTCM via correction connection, but configured as base station!");
  }
}

bool UbloxGps::_setMode(ublox_rtk::SetRtkModeRequest& req, ublox_rtk::SetRtkModeResponse& resp) {

  if (req.mode == req.RTK_MODE_BASE) {
    _setRover(false);
  } else if (req.mode == req.RTK_MODE_ROVER) {
    _setRover(true);
  } else {
    ROS_ERROR_STREAM("Unknown RTK mode request: " << static_cast<int>(req.mode) << ", skipping");
    return false;
  }
  return true;
}

void UbloxGps::_setRover(bool rover) {

  DS_D(UbloxGps);
  ublox::UbloxConfig config(d->config_layer);

  d->rover = rover;
  uint8_t rtcm_rate = 0;

  if (!rover) {
    rtcm_rate = d->rtcm_downsample;
  }

  ROS_INFO_STREAM("Setting RTCM rate to " <<static_cast<int>(rtcm_rate));
  // enable RTCM output
  config.addConfigValue<uint8_t>(UBX_OUT_RTCM_TYPE1005 + d->host_port, rtcm_rate);
  config.addConfigValue<uint8_t>(UBX_OUT_RTCM_TYPE1074 + d->host_port, rtcm_rate);
  config.addConfigValue<uint8_t>(UBX_OUT_RTCM_TYPE1077 + d->host_port, rtcm_rate);
  config.addConfigValue<uint8_t>(UBX_OUT_RTCM_TYPE1084 + d->host_port, rtcm_rate);
  config.addConfigValue<uint8_t>(UBX_OUT_RTCM_TYPE1087 + d->host_port, rtcm_rate);
  config.addConfigValue<uint8_t>(UBX_OUT_RTCM_TYPE1094 + d->host_port, rtcm_rate);
  config.addConfigValue<uint8_t>(UBX_OUT_RTCM_TYPE1097 + d->host_port, rtcm_rate);
  config.addConfigValue<uint8_t>(UBX_OUT_RTCM_TYPE1124 + d->host_port, rtcm_rate);
  config.addConfigValue<uint8_t>(UBX_OUT_RTCM_TYPE1127 + d->host_port, rtcm_rate);
  config.addConfigValue<uint8_t>(UBX_OUT_RTCM_TYPE1230 + d->host_port, rtcm_rate);

  uint32_t min_dur = static_cast<uint32_t>(d->svin_duration);
  uint32_t acc_limit = static_cast<uint32_t>(d->svin_accuracy*1.0e4);

  if (!rover) {
    ROS_INFO_STREAM("setRover starting SURVEYIN");
    config.addConfigValue<uint8_t>(UBX_CFG_TMODE,
                                   UBX_CFG_TMODE_SURVEY_IN);
    config.addConfigValue<uint32_t>(UBX_CFG_TMODE_SVIN_ACC_LIMIT, acc_limit);
    config.addConfigValue<uint32_t>(UBX_CFG_TMODE_SVIN_MIN_DUR, min_dur);
  } else {
    ROS_INFO_STREAM("setRover DISABLING surveyin");
      config.addConfigValue<uint8_t>(UBX_CFG_TMODE,
                                     UBX_CFG_TMODE_DISABLED);
  }

  // fill in length, checksum, etc
  if (d->config_layer != 0) { // only send if not "none"
    connection("instrument")->send(config.toMessage());
  }

  if (!rover) {
    /*
    // this works, but its probably a semantic error-- it doesnt' do what you want it to.
    ROS_INFO_STREAM("Sending RESET to clear SURVEYIN position...");
    ubx_reset_cmd cmd;
    cmd.hdr.sync1 = UBX_SYNC1;
    cmd.hdr.sync2 = UBX_SYNC2;
    cmd.hdr.msg_class = UBX_CFG_CLASS;
    cmd.hdr.msg_id = UBX_CFG_ID_RESET;
    cmd.hdr.payload_size = htole16(4);
    // clear position and local clock parameters, but NOT the satellite info
    cmd.navBbrMask = htole16(UBX_RESET_MASK_POS | UBX_RESET_MASK_OSC | UBX_RESET_MASK_RTC);
    //cmd.navBbrMask = UBX_RESET_MASK_STARTCOLD;
    cmd.resetMode = UBX_RESET_MODE_SOFTWARE_GNSS_ONLY;
    cmd.reserved1 = 0;
    cmd.chksum = ubx_calc_checksum(reinterpret_cast<uint8_t*>(&cmd), sizeof(ubx_reset_cmd)-2);
    std::string cmdstr(reinterpret_cast<char*>(&cmd), sizeof(ubx_reset_cmd));

    connection("instrument")->send(cmdstr);
     */
  }
}

void UbloxGps::_handleRtcm(const ds_core_msgs::RawData& bytes) {
  DS_D(UbloxGps);
  if (!d->rover) {
    if (!d->corrections) {
      ROS_WARN_STREAM("Unable to send out corrections on port...");
      return;
    }
    //ROS_INFO_STREAM("RTCM: passing on " <<bytes.data.size() <<" bytes!");
    d->corrections->send(std::string(bytes.data.begin(), bytes.data.end()));
  } else {
    ROS_WARN_ONCE("Received RTCM corrections from device, but configured as rover!");
  }
}

} // namespace ds_sensors
