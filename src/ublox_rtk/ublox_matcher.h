//
// Created by ivaughn on 8/18/19.
//

#include <deque>
#include <ds_asio/ds_asio.h>

#include "ubx.h"
#include "rtcm.h"
#include <endian.h>

#ifndef RTK_UBLOX_MATCHER_H_
#define RTK_UBLOX_MATCHER_H_

/// @brief Generates a matcher class for async_read_until. This class makes async_read_until return when the binary
/// header is identified and the binary data received matches the expected length
///
/// @return iterator and true pair if the data packet is complete, iterator and false pair otherwise
///
class ublox_matcher
{
 public:
  explicit ublox_matcher() {
    state = NOSYNC;
    length = -1;
  }

  // enum for the current state of the matcher
  enum State {
      NOSYNC=0,
      NMEA,
      UBX,
      RTCM
  };

  template <typename Iterator>
  std::pair <Iterator, bool> match_nosync(Iterator begin, Iterator end) {
    // look for the start of ANY packet
    Iterator iter = begin;

    length = -1;
    while (iter != end) {
      if (*iter == '$' || *iter == UBX_SYNC1_SIGNED || *iter == RTCM_SYNC_SIGNED) {
        // this is the START of the next message and should NOT be consumed
        return std::make_pair(iter, true);
      }
      iter++;
    }
    return std::make_pair(iter, false);
  }

  template <typename Iterator>
  std::pair<Iterator, bool> match_nmea(Iterator begin, Iterator end) {
    Iterator iter = begin;
    while(iter != end) {
      if (*iter++ == '\n') {
        state = NOSYNC;
        return std::make_pair(iter, true);
      }
    }
    return std::make_pair(iter, false);
  }

  template <typename Iterator>
  std::pair<Iterator, bool> match_ubx(Iterator begin, Iterator end) {
    Iterator iter = begin;
    // we haven't gotten the header, so read that
    while (length < 0 && iter != end) {
      header_.push_back(*iter);
      iter++;

      // we Just finally got the filled!
      if (header_.size() >= sizeof(ubx_hdr)) {
        ubx_hdr hdr;
        uint8_t *ptr = reinterpret_cast<uint8_t *>(&hdr);
        // copy the header
        for (size_t i = 0; i < sizeof(ubx_hdr); i++) {
          *ptr = header_[i];
          ptr++;
        }
        header_.clear();

        // get the size
        length = le16toh(hdr.payload_size) + UBX_CHECKSUM_LEN;
      } // if we just got a full header
    } // while iter != end

    // ok, now just read, decrementing length, until you have 0 bytes left
    while (length > 0 && iter != end) {
      length--;
      iter++;
    }

    if (length == 0) {
      // we made it! reset length, start over
      length = -1;
      state = NOSYNC;
      return std::make_pair(iter, true);
    }
    return std::make_pair(iter, false);
  }

  template <typename Iterator>
  std::pair<Iterator, bool> match_rtcm(Iterator begin, Iterator end) {
    Iterator iter = begin;
    // we haven't gotten the header, so read that
    while (length < 0 && iter != end) {
      header_.push_back(*iter);
      iter++;

      // we Just finally got the filled!
      if (header_.size() >= sizeof(rtcm_hdr)) {
        rtcm_hdr hdr;
        uint8_t *ptr = reinterpret_cast<uint8_t*>(&hdr);
        // copy the header
        for (size_t i = 0; i < sizeof(rtcm_hdr); i++) {
          *ptr = header_[i];
          ptr++;
        }
        header_.clear();

        // get the size
        length = (be16toh(hdr.length) & 0x03ff) + RTCM_CHECKSUM_LEN;
      } // if we just got a full header
    } // while iter != end

    // ok, now just read, decrementing length, until you have 0 bytes left
    while (length > 0 && iter != end) {
      length--;
      iter++;
    }

    if (length == 0) {
      // we made it! reset length, start over
      length = -1;
      state = NOSYNC;
      return std::make_pair(iter, true);
    }
    return std::make_pair(iter, false);
  }

  State choose_next_state(const char c) {
    bool ret = false;
    switch (c) {
      case '$':
        return NMEA;
      case UBX_SYNC1_SIGNED:
        return UBX;
      case RTCM_SYNC_SIGNED:
        return RTCM;
      default:
        return NOSYNC;
    }
  }

  template <typename Iterator>
  std::pair<Iterator, bool> operator()(Iterator begin, Iterator end) {
    // if we are unsynch'd its probably because we just finished the previous packet.
    // try to start the next one
    if (state == NOSYNC) {
      state = choose_next_state(*begin);
    }

    // state transitions are handled by jumping to the next state if
    switch (state) {
      case NOSYNC:
        // we're still syncing
        return match_nosync(begin, end);

      case NMEA:
        // its NMEA! Look for that terminating \n
        return match_nmea(begin, end);

      case UBX:
        // UBX's proprietary binary protocol.  Parse the header!
        return match_ubx(begin, end);

      case RTCM:
        // RTCM real-time kinematic corrections
        return match_rtcm(begin, end);

      default:
        ROS_FATAL_STREAM("Matcher in invalid state!");
    }

    return std::make_pair(end, false);
  }

  State getState() const { return state; }
  ssize_t getLength() const { return length; }

 private:
  State state;

  ssize_t length;
  std::deque<uint8_t> header_;
};

#endif //RTK_UBLOX_MATCHER_H_
