//
// Created by ivaughn on 8/22/19.
//

#include <gtest/gtest.h>
#include <boost/asio.hpp>
#include <iostream>

#include "../src/ublox_rtk/ublox_matcher.h"

TEST(UbloxMatcher, nmea) {
  auto m = ublox_matcher();

  boost::asio::streambuf buffer;
  std::ostream bs(&buffer);
  bs <<"$some, nmea, test\r\n";

  auto r = m(boost::asio::buffers_begin(buffer.data()), boost::asio::buffers_end(buffer.data()));

  EXPECT_EQ(boost::asio::buffers_end(buffer.data()), r.first);
  EXPECT_TRUE(r.second); // is true

  // check postcondition
  EXPECT_EQ(-1, m.getLength());
  EXPECT_EQ(ublox_matcher::NOSYNC, m.getState());
}

TEST(UbloxMatcher, nmea_twice) {
  auto m = ublox_matcher();

  boost::asio::streambuf buffer;
  std::ostream bs(&buffer);
  bs <<"$some, nmea, test\r\n$some, nmea, test\r\n";

  auto r = m(boost::asio::buffers_begin(buffer.data()), boost::asio::buffers_end(buffer.data()));

  EXPECT_EQ('$', *r.first);
  EXPECT_TRUE(r.second);
  EXPECT_EQ(19, r.first - boost::asio::buffers_begin(buffer.data()) );

  // check postcondition
  EXPECT_EQ(-1, m.getLength());
  EXPECT_EQ(ublox_matcher::NOSYNC, m.getState());
}

TEST(UbloxMatcher, nosync_junk) {
  auto m = ublox_matcher();

  boost::asio::streambuf buffer;
  std::ostream bs(&buffer);
  bs <<";laskdhjf;aklihbga$some, nmea, test\r\n";

  auto r = m(boost::asio::buffers_begin(buffer.data()), boost::asio::buffers_end(buffer.data()));

  EXPECT_EQ('$', *r.first);
  EXPECT_TRUE(r.second); // is true

  // check postcondition
  EXPECT_EQ(-1, m.getLength());
  EXPECT_EQ(ublox_matcher::NOSYNC, m.getState());
}

TEST(UbloxMatcher, interrupted_nmea) {
   auto m = ublox_matcher();

  boost::asio::streambuf buffer;
  std::ostream bs(&buffer);
  bs <<"$some, nm";

  auto r = m(boost::asio::buffers_begin(buffer.data()), boost::asio::buffers_end(buffer.data()));

  EXPECT_EQ(boost::asio::buffers_end(buffer.data()), r.first);
  EXPECT_FALSE(r.second);
  EXPECT_EQ(-1, m.getLength());
  EXPECT_EQ(ublox_matcher::NMEA, m.getState());

  bs <<"ea, test\r\n";

  r = m(boost::asio::buffers_begin(buffer.data()), boost::asio::buffers_end(buffer.data()));
  EXPECT_EQ(boost::asio::buffers_end(buffer.data()), r.first);
  EXPECT_TRUE(r.second);
  EXPECT_EQ(-1, m.getLength());
  EXPECT_EQ(ublox_matcher::NOSYNC, m.getState());
}

TEST(UbloxMatcher, ubx) {
  auto m = ublox_matcher();

  boost::asio::streambuf buffer;
  std::ostream bs(&buffer);
  unsigned char packet[] = {0xb5, 0x62, 0x01, 0x02, 0x04, 0x00, 0x41, 0x42, 0x43, 0x44, 0x21, 0x22};
  bs.write(reinterpret_cast<char*>(packet), sizeof(packet));

  auto r = m(boost::asio::buffers_begin(buffer.data()), boost::asio::buffers_end(buffer.data()));

  EXPECT_EQ(boost::asio::buffers_end(buffer.data()), r.first);
  EXPECT_TRUE(r.second);
  EXPECT_EQ(-1, m.getLength());
  EXPECT_EQ(ublox_matcher::NOSYNC, m.getState());
}

int main(int argc, char** argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}